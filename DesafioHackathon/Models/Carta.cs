﻿using System;
using System.Threading.Tasks;
namespace DesafioHackathon.Models
{
    public class Carta
    {
      
        
     public string Endereco1 { get; set;  }
     public string Nome1 { get; set; }
     public string NumeroContato1 { get; set; }
        
        
    public int Id { get; set; }

    public string Mensagem { get; set; }

        public Task WriteMessage(string Mensagem)
        {
            Console.WriteLine(
                $"Carta.WriteMessage called. Message: {Mensagem}");

            return Task.FromResult(0);
        }

        public Carta()
        {
        }
    }
}